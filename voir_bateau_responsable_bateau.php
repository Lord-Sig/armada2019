<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Accueil</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/thumbnail-gallery.css" rel="stylesheet">

  <!-- Mon css perso-->
  <link href="css_global.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand-perso" href="#">Armada</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="pageconnexion.html">Retour
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pageinscription.html">Gestion de compte
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href=>Déconnexion "mettre php Déconnexion"
              <span class="sr-only">(current)</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4 text-center text-lg-left">Armada 2019</h1>
    <div class="card-group">
      <div class="card">
        <img class="card-img-top" src="image/bateau/196bf65b05957ffa49ab7a5207b10ab8754c8ce4.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">"mettre nom bateau php"</h5>
          <p class="card-text">"mettre resumé bateau php"</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago"add php"</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="image/bateau/2e04f6f18c069fd5ac554809ededd4084aaebd39.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">"mettre nom bateau php"</h5>
          <p class="card-text">"mettre resumé bateau php"</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago"add php"</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="image/bateau/44e84a2d64cfac788f43fc4b063851bb5f618a76.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">"mettre nom bateau php"</h5>
          <p class="card-text">"mettre resumé bateau php"</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago"add php"</small></p>
        </div>
      </div>
    </div>
    <div class="card-group">
      <div class="card">
        <img class="card-img-top" src="image/bateau/5724759a25ab23f8d964853c1559078beee295b3.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">"mettre nom bateau php"</h5>
          <p class="card-text">"mettre resumé bateau php"</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago"add php"</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="image/bateau/5f23b8eed4f93e6babcd3ef9994936fb74fe1d4e.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">"mettre nom bateau php"</h5>
          <p class="card-text">"mettre resumé bateau php".</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago"add php"</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="image/bateau/7c44a786fa4102589a809b5048c8496c196574e6.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">"mettre nom bateau php"</h5>
          <p class="card-text">"mettre resumé bateau php"</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago"add php"</small></p>
        </div>
      </div>
    </div>


  </div>
  <!-- /.container -->


  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>