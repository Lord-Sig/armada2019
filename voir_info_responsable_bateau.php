<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>"mettre nom du bateau"</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/thumbnail-gallery.css" rel="stylesheet">

  <!-- Mon css perso-->
  <link href="css_global.css" rel="stylesheet">
  <link href="ccs_voir_information_visiteur.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand-perso" href="#">Armada</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="pageconnexion.html">Retour
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="pageinscription.html">Gestion de compte
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href=>Déconnexion "mettre php Déconnexion"
              <span class="sr-only">(current)</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Page Content -->
  <div class="container">

    <!-- Portfolio Item Heading -->
    <h1 class="my-4">
      <small>"mettre nom du bateau"</small>
    </h1>

    <!-- Portfolio Item Row -->
    <div class="container-fluid">
      <div class="row">
        <!-- image -->
        <div class="col-md-8 ">
          <img src="image/bateau/2e04f6f18c069fd5ac554809ededd4084aaebd39.jpg" class="img-fluid" alt="image bateau en php">
        </div>

        <div class="col-md-4  ">
          <h3 class="my-3">Information du "mettre le nom du bateau"</h3>
          <ul>
            <li>Id du tableau :</li>
            <li>Nom du bateau :</li>
            <li>Nationalité :</li>
            <li>Taille du bateau</li>
            <li>Nombre d'équipage :</li>
            <li>Port d'attache :</li>
            <li>Gréement :</li>
            <li>Année de lancement :</li>
          </ul>
          <h3 class="my-3">Télécharger le pdf</h3>
          <button type="button" class="btn btn-primary">Téléchargement</button>
          <h3 class="my-3">Modification des informations </h3>
          <a href="formulaire_changement_info_sommaire.html" class="btn btn-primary btn-lg active" role="button"
            aria-pressed="true">Informations sommaire</a>
          <p><br /></p>
          <a href="formulaire_changement_info_détail.html" class="btn btn-primary btn-lg active" role="button"
            aria-pressed="true">Informations détailler</a>
        </div>

      </div>
    </div>
  </div>

  <!-- /.container -->

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>