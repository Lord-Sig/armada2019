<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>"mettre nom du bateau"</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">

    <!-- Mon css perso-->
    <link href="css_global.css" rel="stylesheet">
    
    <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <link href="ccs_voir_information_visiteur.css" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand-perso" href="#">Armada</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="pageconnexion.html">Retour
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="pageinscription.html">Gestion de compte
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=>Déconnexion "mettre php Déconnexion"
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->

    <div class="container">

                <h1>Information sommaire</h1>

                <fieldset>
                <form action="sqlphp/sql_nameboat/send_info_nameboat.php" method="post">
                    <label for="name">Nom du bateau :</label>
                    <input type="text" id="nameboat" name="nameboat">
                    
                    <label for="text">Nationalité :</label>
                    <input type="text" id="nationalite" name="nationalite">
                    <label for="text">Taille du bateau :</label>
                    <input type="text" id="size_boat" name="size_boat">
                    <label for="text">Nombre d'équipage :</label>
                    <input type="text" id="size_crew" name="size_crew">
                    <label for="text">Port d'attache :</label>
                    <input type="text" id="Harbor" name="Harbor">
                    <label for="text">Gréement:</label>
                    <input type="text" id="greement" name="greement">
                    <label for="text">Année de lancement :</label>
                    <input type="text" id="year_start" name="year_start">
                    <label for="text">Résumé sommaire:</label>
                    <textarea id="resume" name="resume"></textarea>
                    <label for="bio">Histoire du bateau 9999 caractère max:</label>
                    <textarea id="storybateau" name="storybateau"></textarea>
                    <label for="name">Image sous forme de lien vers un herbergeur :</label>
                    <input type="text" id="image" name="image">
                    <input type="submit" value="Valider" />
                </form>
                </fieldset>
    </div>
    <!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>