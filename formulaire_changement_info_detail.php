<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>"mettre nom du bateau"</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/thumbnail-gallery.css" rel="stylesheet">

    <!-- Mon css perso-->
    <link href="css_global.css" rel="stylesheet">
    
    <link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <link href="ccs_voir_information_visiteur.css" rel="stylesheet">

</head>
 
<body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand-perso" href="#">Armada</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="pageconnexion.html">Retour
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="pageinscription.html">Gestion de compte
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=>Déconnexion "mettre php Déconnexion"
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->

    <div class="container">

 <h1>Information détailler </h1>

<fieldset>

    <p>
<form action="sqlphp/sql_image/send_info_image.php" method="post">
        <label for="name">Image sous forme de lien vers un herbergeur :</label>
        <input type="text" id="image" name="image">
        <input type="submit" value="Valider" />
</form> 
    </p>

    <p>
<form action="sqlphp/sql_story/send_info_story.php" method="post">
        <label for="bio">Histoire du bateau 9999 caractère max:</label>
        <textarea id="storybateau" name="storybateau"></textarea>
        <input type="submit" value="Valider" />
</form>
    </p>
</fieldset>
</form>
<!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>