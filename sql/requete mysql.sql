CREATE TABLE Info (
    Idbateau INTEGER NOT NULL AUTO_INCREMENT,
    nom_du_bateau VARCHAR(40),
    nationalite VARCHAR(40),
    taille_bateau VARCHAR(40),
    nombre_equipage VARCHAR(40),
    port_attache VARCHAR(100),
    greement VARCHAR(40),
    image  VARCHAR(200), 
    histoire VARCHAR(9999),
    resume VARCHAR(240),
    annee_de_lancement VARCHAR(40),
    idpropio INTEGER,
    PRIMARY KEY (Idbateau)
)  