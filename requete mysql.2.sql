CREATE TABLE Info_sommaire (
    Idbateau INTEGER NOT NULL AUTO_INCREMENT,
    nom_du_bateau VARCHAR(40),
    nationalite VARCHAR(40),
    taille_bateau VARCHAR(40),
    nombre_equipage VARCHAR(40),
    port_attache VARCHAR(40),
    greement VARCHAR(40),
    image  VARCHAR(40), 
    histoire VARCHAR(9999),
    annee_de_lancement VARCHAR(40),
    PRIMARY KEY (Idbateau)
)