<?php
if(!isset($_POST['nationalite']))
{
    header('Location: ../../formulaire_changement_info_sommaire.php');
}
else
{
    // On va vérifier les variables
    require('formulair_changement_info_nationalite_commande_sql.inc'); // On réclame le fichier

    if (Changemnt_nationalite($_POST['nationalite']))
    {
                
        // On redirige vers la page suivante
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
    else
    {
        // On redirige vers la page formulaire
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
}
?>