<?php
if(!isset($_POST['nameboat']))
{
    header('Location: ../../formulaire_changement_info_sommaire.php');
}
else
{
    // On va vérifier les variables
    require('formulair_changement_info_nameboat_commande_sql.inc'); // On réclame le fichier

    if (Changemnt_nameboat($_POST['nameboat']))
    {
                
        // On redirige vers la page suivante
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
    else
    {
        // On redirige vers la page formulaire
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
}
?>