<?php
if(!isset($_POST['year_start']))
{
    header('Location: ../../formulaire_changement_info_sommaire.php');
}
else
{
    // On va vérifier les variables
    require('formulair_changement_info_year_start_commande_sql.inc'); // On réclame le fichier

    if (Changemnt_year_start($_POST['year_start']))
    {
                
        // On redirige vers la page suivante
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
    else
    {
        // On redirige vers la page formulaire
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
}
?>