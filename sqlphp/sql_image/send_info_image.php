<?php
if(!isset($_POST['image']))
{
    header('Location: ../formulaire_changement_info_detail.php');
}
else
{
    // On va vérifier les variables
    require('formulair_changement_info_image_commande_sql.inc'); // On réclame le fichier

    if (Changemnt_image($_POST['image']))
    {
                
        // On redirige vers la page suivante
        header('Location: ../../formulaire_changement_info_detail.php');
    }
    else
    {
        // On redirige vers la page formulaire
        header('Location: ../../formulaire_changement_info_detail.php');
    }
}
?>