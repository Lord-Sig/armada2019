<?php
if(!isset($_POST['resume']))
{
    header('Location: ../../formulaire_changement_info_detail.php');
}
else
{
    // On va vérifier les variables
    require('formulair_changement_info_story_resume_sql.inc'); // On réclame le fichier

    if (Changemnt_storybateau($_POST['resume']))
    {
                
        // On redirige vers la page suivante
        header('Location: ../../formulaire_changement_info_detail.php');
    }
    else
    {
        // On redirige vers la page formulaire
        header('Location: ../../formulaire_changement_info_detail.php');
    }
}
?>