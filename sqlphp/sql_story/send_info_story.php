<?php
if(!isset($_POST['storybateau']))
{
    header('Location: ../../formulaire_changement_info_sommaire.php');
}
else
{
    // On va vérifier les variables
    require('formulair_changement_info_story_commande_sql.inc'); // On réclame le fichier

    if (Changemnt_storybateau($_POST['storybateau']))
    {
                
        // On redirige vers la page suivante
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
    else
    {
        // On redirige vers la page formulaire
        header('Location: ../../formulaire_changement_info_sommaire.php');
    }
}
?>